/**
 * 
 */
package io.gitlab.ciskos.ipcrawler.file;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import io.gitlab.ciskos.ipcrawler.web.Path;

public class FileUtil {

	private static final String LINE_FEED = "\r\n";

	public static void certificatesDomainsToFiles(Map<String, List<String>> domainsFromCertificatesList)
			throws IOException {
		if (!domainsFromCertificatesList.isEmpty()) {
			for (Map.Entry<String, List<String>> entry : domainsFromCertificatesList.entrySet()) {
				String key = entry.getKey();
				List<String> value = entry.getValue();
				
				if (!value.isEmpty()) {
					FileUtil.writeDomainsToFile(key, value);
				}
			}
		}
	}
	
	private static void writeDomainsToFile(String address, List<String> domains) throws IOException {
        if (Files.notExists(Paths.get(Path.FileSystem.IP_LIST_TMP))) {
			Files.createDirectory(Paths.get(Path.FileSystem.IP_LIST_TMP));
		}
		
		FileWriter fw = new FileWriter(Path.FileSystem.IP_LIST_TMP + Path.FileSystem.SLASH + address);

		StringBuilder sb = new StringBuilder();
		for (String domain : domains) {
			sb.append(domain + LINE_FEED);
		}

		fw.write(sb.toString());
		fw.close();
	}
	
}
