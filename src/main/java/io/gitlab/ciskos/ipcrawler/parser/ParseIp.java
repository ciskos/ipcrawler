package io.gitlab.ciskos.ipcrawler.parser;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import inet.ipaddr.IPAddress;
import inet.ipaddr.IPAddressString;

public class ParseIp {
	
	public static List<String> parse(String ipSubnet) {
		IPAddress subnet = new IPAddressString(ipSubnet)
										.getAddress().withoutPrefixLength();
		
		return iteratorToList(subnet.iterator());
	}

	private static List<String> iteratorToList(Iterator<? extends IPAddress> iterator) {
		List<String> result = new ArrayList<String>();
		
		while (iterator.hasNext()) {
			result.add(iterator.next().toString());
		}
		
		return result;
	}
	
}
