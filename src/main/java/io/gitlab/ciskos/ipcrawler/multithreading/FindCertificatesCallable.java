package io.gitlab.ciskos.ipcrawler.multithreading;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import io.gitlab.ciskos.ipcrawler.httpclient.HttpClientGetServerCertificate;
import io.gitlab.ciskos.ipcrawler.parser.ParseCertificate;
import lombok.extern.java.Log;

@Log
public class FindCertificatesCallable implements Callable<Map<String,List<String>>> {

	private static final String LOG_MESSAGE = "Сканирование IP-адреса на наличие сертификатов: ";
	private List<String> ipRangeToScan;

	public FindCertificatesCallable(List<String> ipRangeToScan) {
		this.ipRangeToScan = ipRangeToScan;
	}

	@Override
	public Map<String,List<String>> call() throws Exception {
		Map<String,List<String>> domainsInCertificate = new HashMap<>();

		for (String ip : ipRangeToScan) {
			log.info(LOG_MESSAGE + ip);
			
			List<String> certificatesList = HttpClientGetServerCertificate.getCertificatesList(ip);

			for (String certificate : certificatesList) {
				domainsInCertificate.put(ip, ParseCertificate.parse(certificate));
			}
		}
		
		return domainsInCertificate;
	}

}
