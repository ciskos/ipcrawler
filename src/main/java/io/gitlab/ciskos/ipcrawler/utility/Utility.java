package io.gitlab.ciskos.ipcrawler.utility;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import org.apache.commons.collections4.ListUtils;

import io.gitlab.ciskos.ipcrawler.parser.ParseIp;
import lombok.extern.java.Log;

@Log
public class Utility {

	private static final String SUBNET_PATTERN = "(\\d{1,3}\\.){3}(\\d)(\\/[0-2]?[0-9])";
	private static final String THREADS_NUMBER_PATTERN = "\\b\\d{1,3}\\b";
	
	public static List<String> futuresToList(List<Future<List<String>>> futures) {
		return futures.stream()
				.map(f -> {
					List<String> result = null;
					
					try {
						result = f.isDone() ? f.get() : Collections.emptyList();
					} catch (InterruptedException | ExecutionException e) {
						log.info(e.getMessage());
					}
					
					return result;
				})
				.flatMap(List::stream)
				.collect(Collectors.toList());
	}
	
	public static Map<String, List<String>> futuresToMap(List<Future<Map<String, List<String>>>> futures) {
		return futures.stream()
				.map(f -> {
					Map<String,List<String>> result = null;
					
					try {
						result = f.isDone() ? f.get() : Collections.emptyMap();
					} catch (InterruptedException | ExecutionException e) {
						log.info(e.getMessage());
					}
					
					return result;
				})
				.flatMap(m -> m.entrySet().stream())
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}
	
	public static List<List<String>> divideListToSublists(List<String> ipList, Integer numberOfSublists) {
		return ListUtils.partition(ipList, numberOfSublists);
	}
	
	public static List<String> parseIncomingSubnet(String subnet) {
		return ParseIp.parse(subnet);
	}

	public static boolean validateSubnet(String subnet) {
		return subnet.matches(SUBNET_PATTERN);
	}

	public static boolean validateThreadsNumber(String threads) {
		return threads.matches(THREADS_NUMBER_PATTERN);
	}

}
