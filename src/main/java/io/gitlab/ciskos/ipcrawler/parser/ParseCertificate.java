package io.gitlab.ciskos.ipcrawler.parser;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParseCertificate {
	
	private static final String DOMAIN_REGEX =
			"([a-z0-9_\\-]{1,5}:\\/\\/)?(([a-z0-9_\\-]{1,}):([a-z0-9_\\-]{1,})\\@)?((www\\.)|([a-z0-9_\\-]{1,}\\.)+)?([a-z0-9_\\-]{3,})(\\.[a-z]{2,4})(\\/([a-z0-9_\\-]{1,}\\/)+)?([a-z0-9_\\-]{1,})?(\\.[a-z]{2,})?(\\?)?(((\\&)?[a-z0-9_\\-]{1,}(\\=[a-z0-9_\\-]{1,})?)+)?";

	public static List<String> parse(String certificate) {
		Pattern pattern = Pattern.compile(DOMAIN_REGEX);
		Matcher matcher = pattern.matcher(certificate);
		Set<String> result = new HashSet<>();
		
		while (matcher.find()) {
			result.add(matcher.group(0));
		}
		
		return new ArrayList<>(result);
	}
	
}
