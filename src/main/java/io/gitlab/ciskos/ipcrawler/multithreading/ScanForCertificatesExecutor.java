package io.gitlab.ciskos.ipcrawler.multithreading;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import io.gitlab.ciskos.ipcrawler.utility.Utility;

public class ScanForCertificatesExecutor {
	
	public static Map<String,List<String>> getDomainsFromCertificatesList(List<String> ipsList, Integer numberOfLists) throws InterruptedException {
		ExecutorService executorService = Executors.newFixedThreadPool(numberOfLists);
		
		List<Future<Map<String,List<String>>>> downloadedCertificatesList =
				executorService.invokeAll(getCallablesCertificateList(ipsList, numberOfLists));
		
		executorService.shutdown();
		
		return Utility.futuresToMap(downloadedCertificatesList);
	}
	
	private static List<Callable<Map<String,List<String>>>> getCallablesCertificateList(List<String> ipsList, Integer numberOfSublists) {
		List<Callable<Map<String,List<String>>>> callables = new ArrayList<>();
		
		List<List<String>> ipSubLists = Utility.divideListToSublists(ipsList, numberOfSublists);
		
		for (List<String> ipSubList : ipSubLists) {
			callables.add(new FindCertificatesCallable(ipSubList));
		}

		return callables;
	}

}
