package io.gitlab.ciskos.ipcrawler.multithreading;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import lombok.extern.java.Log;

@Log
public class FindReacheableIpsCallable implements Callable<List<String>> {

	private static final String PING_COUNT = "1";
	private static final String C = "-c";
	private static final String N = "-n";
	private static final String PING = "ping";
	private static final String WIN = "win";
	private static final String OS_NAME = "os.name";
	private static final String IP_ACTIVE = "IP-адрес активен: ";
	private static final String IP_NOT_ACTIVE = "IP-адрес неактивен: ";
	private static final String LOG_MESSAGE = "Поиск активного IP-адреса: ";
	private static final String ZERO_OR_255 = "(\\d{1,3}.){3}(0|255)";
	private static final int PING_TIMEOUT = 1000;
	private List<String> ipRangeToScan;

	public FindReacheableIpsCallable(List<String> ipRangeToScan) {
		this.ipRangeToScan = ipRangeToScan;
	}

	@Override
	public List<String> call() throws Exception {
		List<String> reachableIps = new ArrayList<>();

		for (String ip : ipRangeToScan) {
			log.info(LOG_MESSAGE + ip);
			
			if (ip.matches(ZERO_OR_255) || !ping(ip)) {
				log.info(IP_NOT_ACTIVE + ip);
				continue;
			}

			log.info(IP_ACTIVE + ip);
			reachableIps.add(ip);
		}

		return reachableIps;
	}

	private static Boolean ping(String host) throws IOException, InterruptedException {
        boolean isWindows = System.getProperty(OS_NAME).toLowerCase().contains(WIN);

        ProcessBuilder processBuilder = new ProcessBuilder(PING, isWindows ? N : C, PING_COUNT, host);
        Process proc = processBuilder.start();
        return proc.waitFor(PING_TIMEOUT, TimeUnit.MILLISECONDS);
    }

}
