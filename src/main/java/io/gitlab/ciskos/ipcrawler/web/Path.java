package io.gitlab.ciskos.ipcrawler.web;

public class Path {
	
	public static class FileSystem {
		public static final String SLASH = "/";
		public static final String SYSTEM_TMP_DIRECTORY_VARIABLE = "java.io.tmpdir";
		public static final String IP_LIST_DIRECTORY = "iplist";
		public static final String SYSTEM_TMP = System.getProperty(SYSTEM_TMP_DIRECTORY_VARIABLE);
		public static final String IP_LIST_TMP = SYSTEM_TMP + SLASH + IP_LIST_DIRECTORY;
	}
	
	public static class Web {
		public static final String ROOT = "/";
		public static final String SUBNET = "/subnet";
		public static final String SCAN_STATUS = "/status";
	}

    public static class Template {
    	public static final String INDEX = "index.html";
    }

}
