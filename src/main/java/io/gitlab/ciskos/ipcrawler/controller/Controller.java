package io.gitlab.ciskos.ipcrawler.controller;

import java.util.List;
import java.util.Map;

import io.gitlab.ciskos.ipcrawler.file.FileUtil;
import io.gitlab.ciskos.ipcrawler.multithreading.ScanForActiveIpsExecutor;
import io.gitlab.ciskos.ipcrawler.multithreading.ScanForCertificatesExecutor;
import io.gitlab.ciskos.ipcrawler.utility.Utility;
import io.gitlab.ciskos.ipcrawler.web.Path;
import io.javalin.http.Handler;

public class Controller {

	private static final String FORM_PARAM_THREADS = "threads";
	private static final String FORM_PARAM_SUBNET = "subnet";
	private static final Integer DEFAULT_THREADS_NUMBER = 10;

	public static Handler root = ctx -> {
		ctx.redirect(Path.Template.INDEX);
	};

	public static Handler receiveSubnet = ctx -> {
		String subnetParam = ctx.formParam(FORM_PARAM_SUBNET);
		String threadsParam = ctx.formParam(FORM_PARAM_THREADS);
		Integer threadsNum = Utility.validateThreadsNumber(threadsParam)
				? Integer.parseInt(threadsParam) : DEFAULT_THREADS_NUMBER;

		if (Utility.validateSubnet(subnetParam)) {
			List<String> ipListToScan =
					ScanForActiveIpsExecutor.activeIpsList(subnetParam, threadsNum);

			if (!ipListToScan.isEmpty()) {
				Map<String, List<String>> domainsFromCertificatesList =
						ScanForCertificatesExecutor
						.getDomainsFromCertificatesList(ipListToScan, threadsNum);

				if (!domainsFromCertificatesList.isEmpty()) {
					FileUtil.certificatesDomainsToFiles(domainsFromCertificatesList);
				}
			}
		}

		ctx.redirect(Path.Template.INDEX);
		ScanForActiveIpsExecutor.resetSubnetScanningStatus();
	};
	
	public static Handler getIpScanningStatus = ctx -> {
		ctx.json(ScanForActiveIpsExecutor.getSubnetScanningStatus());
	};

}
