package io.gitlab.ciskos.ipcrawler.multithreading;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import io.gitlab.ciskos.ipcrawler.utility.Utility;

public class ScanForActiveIpsExecutor {
	
	/*
	 * Статусы:
	 * 1. -1 - сканирование не производится
	 * 2.  0 - сканирование запущено
	 * 3.  1 - сканирование завершено
	 * */
	private static Integer allDone = -1;
	
	public static List<String> activeIpsList(String subnet, Integer numberOfLists) throws InterruptedException, ExecutionException {
		allDone = 0;
		ExecutorService executorService = Executors.newFixedThreadPool(numberOfLists);
		
		List<Callable<List<String>>> callablesIpsList = getCallablesIpsList(subnet, numberOfLists);
		
		List<Future<List<String>>> reacheableIpsList =
				executorService.invokeAll(callablesIpsList);
		
		executorService.shutdown();
		if (executorService.isTerminated()) {
			allDone = 1;
		}
		
		return Utility.futuresToList(reacheableIpsList);
	}
	
	private static List<Callable<List<String>>> getCallablesIpsList(String subnet, Integer numberOfSublists) {
		List<Callable<List<String>>> callables = new ArrayList<>();
		
		List<String> ipsList = Utility.parseIncomingSubnet(subnet);
		List<List<String>> ipSubLists = Utility.divideListToSublists(ipsList, numberOfSublists);
		
		for (List<String> ipSubList : ipSubLists) {
			callables.add(new FindReacheableIpsCallable(ipSubList));
		}

		return callables;
	}

	public static Integer getSubnetScanningStatus() {
		return allDone;
	}
	
	public static void resetSubnetScanningStatus() {
		allDone = -1;
	}
	
}
