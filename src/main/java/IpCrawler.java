import java.util.function.Consumer;

import io.gitlab.ciskos.ipcrawler.controller.Controller;
import io.gitlab.ciskos.ipcrawler.web.Path;
import io.javalin.Javalin;
import io.javalin.apibuilder.ApiBuilder;
import io.javalin.core.JavalinConfig;
import io.javalin.http.staticfiles.Location;

public class IpCrawler {

	public static void main(String[] args) throws Exception  {
		Consumer<JavalinConfig> conf = config -> {
            config.addStaticFiles("/", Location.CLASSPATH);
			config.addSinglePageRoot(Path.Web.ROOT, Path.Template.INDEX);
        };
        
		Javalin javalinServer = Javalin.create(conf)
	        .start(7070);

        javalinServer.routes(() -> {
        	ApiBuilder.post(Path.Web.SUBNET, Controller.receiveSubnet);
        	ApiBuilder.get(Path.Web.SCAN_STATUS, Controller.getIpScanningStatus);
        });
	}
	
}
