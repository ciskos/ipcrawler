package io.gitlab.ciskos.ipcrawler.httpclient;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;

import org.apache.http.HttpResponseInterceptor;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.ManagedHttpClientConnection;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpCoreContext;
import org.apache.http.ssl.SSLContexts;

public class HttpClientGetServerCertificate {

	private static final String ERROR_MESSAGE = "Ошибка: ";
	private static final String HTTP = "http";
	private static final String HTTPS = "https";
	private static final String CERTIFICATES = "CERTIFICATES";
	private static final Integer TIMEOUT = 1000;

	public static List<String> getCertificatesList(String address) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IOException, URISyntaxException {
		Certificate[] certificates = getCertificates(address);
		
		return certificates == null ? Collections.emptyList()
				: Arrays.asList(certificates).stream()
					.map(Certificate::toString)
					.collect(Collectors.toList());
	}
	
	private static Certificate[] getCertificates(String address)
			throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IOException, URISyntaxException {
		URI uri = getUri(HTTPS, address);
		HttpGet httpget = new HttpGet(uri);
		HttpContext context = new BasicHttpContext();

		try (CloseableHttpClient httpClient = configureHttpClient()) {
			httpClient.execute(httpget, context);
		} catch (Exception e) {
			System.out.println(ERROR_MESSAGE + e.getMessage());
		}

		return (Certificate[]) context.getAttribute(CERTIFICATES);
	}

	private static CloseableHttpClient configureHttpClient()
			throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException {
		HttpResponseInterceptor certificateInterceptor = getCertificateInterceptor();
		Registry<ConnectionSocketFactory> socketFactoryRegistry = acceptAllSSLConnections();
		BasicHttpClientConnectionManager connectionManager =
				new BasicHttpClientConnectionManager(socketFactoryRegistry);
		RequestConfig requestConfig = RequestConfig.custom()
				  .setConnectTimeout(TIMEOUT)
				  .setConnectionRequestTimeout(TIMEOUT)
				  .setSocketTimeout(TIMEOUT).build();
		
		return HttpClients.custom()
				.setDefaultRequestConfig(requestConfig)
				.setConnectionManager(connectionManager)
				.addInterceptorLast(certificateInterceptor)
				.build();
	}

	private static Registry<ConnectionSocketFactory> acceptAllSSLConnections()
			throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException {
		TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;
		SSLContext sslContext = SSLContexts.custom()
				.loadTrustMaterial(null, acceptingTrustStrategy)
				.build();
		SSLConnectionSocketFactory sslsf =
				new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);

		return RegistryBuilder.<ConnectionSocketFactory>create()
				.register(HTTPS, sslsf)
				.register(HTTP, new PlainConnectionSocketFactory())
				.build();
	}

	private static URI getUri(String protocol, String address) throws URISyntaxException {
		return new URIBuilder()
				.setScheme(protocol)
				.setHost(address)
				.build();
	}

	private static HttpResponseInterceptor getCertificateInterceptor() {
		return (httpResponse, context) -> {
			ManagedHttpClientConnection routedConnection =
					(ManagedHttpClientConnection) context
					.getAttribute(HttpCoreContext.HTTP_CONNECTION);
			SSLSession sslSession = routedConnection.getSSLSession();

			if (sslSession != null) {
				Certificate[] certificates = sslSession.getPeerCertificates();
				context.setAttribute(CERTIFICATES, certificates);
			}
		};
	}

}
